###############################################################################
# Inputs
###############################################################################
variable "cost_center_id" {}
variable "hostname" {}
variable "domain" {}
variable "num_cpus" {}
variable "memory" {}
variable "resource_pool_id" {}
variable "datastore_id" {}
variable "guest_id" {}
variable "scsi_type" {}
variable "network_id" {}
variable "network_adapter_type" {}
variable "disk_size" {}
variable "disk_eagerly_scrub" {}
variable "disk_thin_provisioned" {}
variable "template_uuid" {}
variable "ipv4_subnet" {}
variable "ipv4_host" {}
variable "ipv4_netmask_length" {}
variable "ipv4_gateway" {}
variable "ssh_username" {}
variable "ssh_password" {}
variable "timeout" {}
variable "reverse_ipv4_subnet" {}
variable "ipv4_subnet_address" {}
variable "ipv4_host_list" { default = [] }
variable "hostname_list" { default = [] }

###############################################################################
# Provision DNS Server
###############################################################################
resource "vsphere_virtual_machine" "build_server" {
  tags                 = ["${var.cost_center_id}"]
  name                 = "${var.hostname}"
  num_cpus             = "${var.num_cpus}"
  memory               = "${var.memory}"

  resource_pool_id     = "${var.resource_pool_id}"
  datastore_id         = "${var.datastore_id}"
  guest_id             = "${var.guest_id}"
  scsi_type            = "${var.scsi_type}"

  network_interface {
    network_id         = "${var.network_id}"
    adapter_type       = "${var.network_adapter_type}"
  }

  disk {
    name               = "${var.hostname}.vmdk"
    size               = "${var.disk_size}"
    eagerly_scrub      = "${var.disk_eagerly_scrub}"
    thin_provisioned   = "${var.disk_thin_provisioned}"
  }

  clone {
    template_uuid      = "${var.template_uuid}"
    timeout            = "${var.timeout}"

    customize {
      linux_options {
        host_name      = "${var.hostname}"
        domain         = "${var.domain}"
      }

      network_interface {
        ipv4_address   = "${var.ipv4_subnet}.${var.ipv4_host + 1}"
        ipv4_netmask   = "${var.ipv4_netmask_length}"
      }

      ipv4_gateway     = "${var.ipv4_gateway}"
      dns_server_list  = [
        "${var.ipv4_gateway}"
      ]
    }
  }
}

###############################################################################
# Configure DNS Server
###############################################################################
resource "null_resource" "configure_server" {
  connection {
    user        = "${var.ssh_username}"
    password    = "${var.ssh_password}"
    host        = "${vsphere_virtual_machine.build_server.clone.0.customize.0.network_interface.0.ipv4_address}"
    type        = "ssh"
  }

  provisioner "salt-masterless" {
    local_state_tree = "${path.module}/config/salt/master"
    remote_state_tree = "/srv/salt"
  }

  provisioner "local-exec" {
    command = "${path.module}/scripts/prep_named_conf.sh ${path.module} ${var.domain} ${var.reverse_ipv4_subnet} ${var.ipv4_subnet_address} ${var.ipv4_netmask_length}"
  }

  provisioner "file" {
    source = "${path.module}/config/bind/named.conf"
    destination = "/tmp/named.conf"
  }

  provisioner "local-exec" {
    command = "${path.module}/scripts/prep_zone_files.sh ${path.module} ${var.domain} ${var.reverse_ipv4_subnet} ${var.ipv4_host + 1} ${vsphere_virtual_machine.build_server.clone.0.customize.0.network_interface.0.ipv4_address} ${var.hostname}.${var.domain} fwd.${var.domain}.db ${var.reverse_ipv4_subnet}.db"
  }

  provisioner "file" {
    source ="${path.module}/config/bind/fwd.${var.domain}.db"
    destination = "/tmp/fwd.${var.domain}.db"
  }

  provisioner "file" {
    source ="${path.module}/config/bind/${var.reverse_ipv4_subnet}.db"
    destination = "/tmp/${var.reverse_ipv4_subnet}.db"
  }

  provisioner "remote-exec" {
    inline = [
      "set -eou pipefail",
      "sudo chown root:named /tmp/named.conf",
      "sudo chown root:named /tmp/*.db",
      "sudo chmod 640 /tmp/named.conf",
      "sudo chmod 640 /tmp/*.db",
      "sudo rm -rf /etc/named.conf",
      "sudo mv /tmp/named.conf /etc/named.conf",
      "sudo mv /tmp/fwd.${var.domain}.db /var/named/fwd.${var.domain}.db",
      "sudo mv /tmp/${var.reverse_ipv4_subnet}.db /var/named/${var.reverse_ipv4_subnet}.db"
    ]
  }
}

###############################################################################
# Register Hosts - Part 1
###############################################################################
resource "null_resource" "register_hosts_part1" {
  count         = "${length(var.ipv4_host_list)}"
  connection {
    user        = "${var.ssh_username}"
    password    = "${var.ssh_password}"
    host        = "${vsphere_virtual_machine.build_server.clone.0.customize.0.network_interface.0.ipv4_address}"
    type        = "ssh"
  }

  provisioner "remote-exec" {
    inline      = [
      "set -eou pipefail",
      "echo ${element(var.hostname_list, count.index)} IN A ${var.ipv4_subnet}.${element(var.ipv4_host_list, count.index)} > /tmp/fwd.${count.index}.tmp",
      "echo ${element(var.ipv4_host_list, count.index)} IN PTR ${element(var.hostname_list, count.index)}.${var.domain}. > /tmp/rev.${count.index}.tmp"
    ]
  }

  depends_on    = [
    "null_resource.configure_server"
  ]
}

###############################################################################
# Register Hosts - Part 2
###############################################################################
resource "null_resource" "register_hosts_part2" {
  connection {
    user        = "${var.ssh_username}"
    password    = "${var.ssh_password}"
    host        = "${vsphere_virtual_machine.build_server.clone.0.customize.0.network_interface.0.ipv4_address}"
    type        = "ssh"
  }

  provisioner "remote-exec" {
    inline      = [
      "set -eou pipefail",
      "sudo systemctl stop named.service",
      "sudo sh -c 'cat /tmp/fwd.*.tmp >> /var/named/fwd.${var.domain}.db'",
      "sudo sh -c 'cat /tmp/rev.*.tmp >> /var/named/${var.reverse_ipv4_subnet}.db'",
      "sudo rm /tmp/*.tmp",
      "sudo systemctl start named.service"
    ]
  }

  depends_on    = [
    "null_resource.register_hosts_part1"
  ]
}
