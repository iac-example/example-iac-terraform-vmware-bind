###############################################################################
# Provider Configuration
###############################################################################
provider "vsphere" {
  user                     = "${var.vsphere_user}"
  password                 = "${var.vsphere_password}"
  vsphere_server           = "${var.vsphere_server}"

  allow_unverified_ssl     = true
}

###############################################################################
# Retrieve Information from VSphere for Use in Build and Configuration
###############################################################################
module "initialize_datasources" {
  source                   = "./initialize_datasources"

  datacenter_name          = "${var.datacenter}"
  resource_pool_name       = "${var.resource_pool}"
  datastore_name           = "${var.datastore}"
  network_name             = "${var.network}"
  template_name            = "${var.template}"
  cost_center_tag_name     = "${var.cost_center_tag_name}"
  cost_center_name         = "${var.cost_center_name}"
}

###############################################################################
# Provision Bind Servers, Install SaltStack Minion Services, and Apply States
###############################################################################
module "provision_dns" {
  source                   = "./provision_bind"

  cost_center_id           = "${module.initialize_datasources.cost_center_id}"
  hostname                 = "${var.dns_hostname}"
  domain                   = "${var.domain}"
  num_cpus                 = "${var.dns_num_cpus}"
  memory                   = "${var.dns_memory}"
  resource_pool_id         = "${module.initialize_datasources.resource_pool_id}"
  datastore_id             = "${module.initialize_datasources.datastore_id}"
  guest_id                 = "${module.initialize_datasources.guest_id}"
  scsi_type                = "${module.initialize_datasources.scsi_type}"
  network_id               = "${module.initialize_datasources.network_id}"
  network_adapter_type     = "${module.initialize_datasources.network_adapter_type}"
  disk_size                = "${module.initialize_datasources.disk_size}"
  disk_eagerly_scrub       = "${module.initialize_datasources.disk_eagerly_scrub}"
  disk_thin_provisioned    = "${module.initialize_datasources.disk_thin_provisioned}"
  template_uuid            = "${module.initialize_datasources.template_uuid}"
  ipv4_subnet              = "${var.ipv4_subnet}"
  ipv4_host                = "${var.ipv4_host}"
  ipv4_netmask_length      = "${var.ipv4_netmask_length}"
  ipv4_gateway             = "${var.ipv4_gateway}"
  ssh_username             = "${var.ssh_username}"
  ssh_password             = "${var.ssh_password}"
  timeout                  = "${var.dns_timeout}"
  reverse_ipv4_subnet      = "${var.reverse_ipv4_subnet}"
  ipv4_subnet_address      = "${var.ipv4_subnet_address}"
  ipv4_host_list           = "${var.ipv4_host_list}"
  hostname_list            = "${var.hostname_list}"
}
