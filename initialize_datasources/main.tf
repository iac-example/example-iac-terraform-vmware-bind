###############################################################################
# Inputs
###############################################################################
variable "datacenter_name" {}
variable "resource_pool_name" {}
variable "datastore_name" {}
variable "network_name" {}
variable "template_name" {}
variable "cost_center_tag_name" {}
variable "cost_center_name" {}

###############################################################################
# Data Source Configurations
###############################################################################
data "vsphere_datacenter" "datacenter" {
  name          = "${var.datacenter_name}"
}

data "vsphere_resource_pool" "resource_pool" {
  name          = "${var.resource_pool_name}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

data "vsphere_datastore" "datastore" {
  name          = "${var.datastore_name}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

data "vsphere_network" "network" {
  name          = "${var.network_name}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "${var.template_name}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

data "vsphere_tag_category" "cost_center" {
  name          = "${var.cost_center_tag_name}"
}

data "vsphere_tag" "cost_center" {
  name          = "${var.cost_center_name}"
  category_id   = "${data.vsphere_tag_category.cost_center.id}"
}

###############################################################################
# Outputs
###############################################################################
output "cost_center_id" {
  value         = "${data.vsphere_tag.cost_center.id}"
}

output "resource_pool_id" {
  value         = "${data.vsphere_resource_pool.resource_pool.id}"
}

output "datastore_id" {
  value         = "${data.vsphere_datastore.datastore.id}"
}

output "guest_id" {
  value         = "${data.vsphere_virtual_machine.template.guest_id}"
}

output "scsi_type" {
  value         = "${data.vsphere_virtual_machine.template.scsi_type}"
}

output "network_id" {
  value         = "${data.vsphere_network.network.id}"
}

output "network_adapter_type" {
  value         = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
}

output "disk_size" {
  value         = "${data.vsphere_virtual_machine.template.disks.0.size}"
}

output "disk_eagerly_scrub" {
  value         = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
}

output "disk_thin_provisioned" {
  value         = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
}

output "template_uuid" {
  value         = "${data.vsphere_virtual_machine.template.id}"
}
